/**
 * [se le pasan dos numeros y el operador]
 * @param  {[int]}  [description]
 * @return {[int]}      [description]
 */
function opera(num1, operador, num2){
	let r=0;
	if (operador=="+"){
		r=((parseInt(num1))+(parseInt(num2)));
	}
	if (operador=="-"){
		r=((parseInt(num1))-(parseInt(num2)));
		}
	if (operador=="/"){
		r=((parseInt(num1))/(parseInt(num2)));
		}
	if (operador=="*"){
		r=((parseInt(num1))*(parseInt(num2)));
		}
	return r;
	//no se recomienda usar eval
	//return eval(num1+operador+num2);
}
